//
// ────────────────────────────────────────────────────────────────────────────────────── I ──────────
//   :::::: D O U B L E C L I C K   B O I L E R P L A T E : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────────────────────────
//

document.addEventListener( 'DOMContentLoaded', function () {

	if ( Enabler.isInitialized() ) {
		enablerInitHandler();
	} else {
		Enabler.addEventListener( studio.events.StudioEvent.INIT, enablerInitHandler );
	}

	function enablerInitHandler () {

		if ( Enabler.isPageLoaded() ) {
			pageLoadedHandler();
		} else {
			Enabler.addEventListener( studio.events.StudioEvent.PAGE_LOADED, pageLoadedHandler );
		}
	}

	function pageLoadedHandler () {

		if ( Enabler.isVisible() ) {
			adVisibilityHandler();
		} else {
			Enabler.addEventListener( studio.events.StudioEvent.VISIBLE, adVisibilityHandler );
		}
	}

	function adVisibilityHandler () {

		Dynamic.init();
		DOM.init();
		Creative.init();
	}
} );

//
// ────────────────────────────────────────────────────── I ──────────
//   :::::: D Y N A M I C : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────
// Dynamic invocation code.
// Use "Dynamic.get().key" to access the value.
// ie: var foo = Dynamic.get().ID;

var Dynamic = ( function () {

	function init () {
        
		Enabler.setProfileId( 10625924 );
		var devDynamicContent = {};
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1 = [ {} ];
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0]._id = 0;
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].logo_img = {};
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].logo_img.Type = 'file';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].logo_img.Url = '../dynamic_assets/enterprise/160x600/logo.png';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f1_bg_img = {};
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f1_bg_img.Type = 'file';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f1_bg_img.Url = '../dynamic_assets/enterprise/160x600/bg1.jpg';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f2_bg_img = {};
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f2_bg_img.Type = 'file';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f2_bg_img.Url = '../dynamic_assets/enterprise/160x600/bg2.jpg';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f3_icon_img = {};
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f3_icon_img.Type = 'file';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f3_icon_img.Url = '../dynamic_assets/enterprise/160x600/icon.png';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f1_copy1 = 'Innovate your <br> payment experiences';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f1_copy2 = 'Become a <br> payments <br> leader';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f2_copy = 'Corporate <br> Payments <br> <span> from Finastra </span> <br><br> Become a <br> payments leader';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].logo_img_config = '{xPos : 18px, yPos : 35px}';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f1_bg_img_config = '{xPos : 0px, yPos : 0px}';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f2_bg_img_config = '{xPos : 0px, yPos : 0px}';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f3_icon_img_config = '{width : 107px, xPos : 20px, yPos : 144px}';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f1_copy1_config = '{txtSize : 20px, txtColor : #FFFFFF, xPos : 10px, yPos : 328px}';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f1_copy2_config = '{txtSize : 20px, txtColor : #FFFFFF, xPos : 10px, yPos : 0px}';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].f2_copy_config = '{txtSize : 20px, txtColor : #FFFFFF, colorAlt : #c137a2, xPos : 15px, yPos : 288px}';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].cta_copy = 'Learn More';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].cta_config = '{bgColor : #c137a2, txtSize : 16px, txtColor : #FFFFFF, xPos : 12px, yPos : 535px, xPad : 25px, yPad : 10px}';
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].exit_url = {};
		devDynamicContent.Finastra_Accelerate_Feed_Sheet_1[0].exit_url.Url = 'https://www.google.com';
		Enabler.setDevDynamicContent( devDynamicContent );
	}

	function get () {
        
		return dynamicContent.Finastra_Accelerate_Feed_Sheet_1[0];
	}

	return {
		init : init,
		get : get
	};
}() );

//
// ──────────────────────────────────────────────────────────────── I ──────────
//   :::::: C R E A T I V E   D O M : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────
// Setup your creative's DOM access here.
// Use "DOM.get().selectorName" to access the value.
// ie: DOM.get().wrapper;

var DOM = ( function () {

	var el = {};

	function init () {

		el.wrapper = Utils.selector( '.main-wrapper' );
		el.logoImg = Utils.selector( '.logo-img img' );
		el.iconImg = Utils.selector( '.icon-img img' );
		el.f1BgImg = Utils.selector( '.f1-bg-img' );
		el.f1Copy1 = Utils.selector( '.f1-copy1' );
		el.f1Copy2 = Utils.selector( '.f1-copy2' );
		el.f2BgImg = Utils.selector( '.f2-bg-img' );
		el.f2Copy = Utils.selector( '.f2-copy' );
		el.cta = Utils.selector( '.cta' );
		el.pixels = Utils.selector( '.pixels img' );
		el.bgHotspot = Utils.selector( '.bg-hotspot' );
	}

	function get () {

		return el;
	}

	return {
		init : init,
		get : get
	};

}() );

//
// ──────────────────────────────────────────────────────── I ──────────
//   :::::: C R E A T I V E : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────
// This is the main entry point of your creative.
// Creative logic, animation, events, and everything to make your creative work.

var Creative = ( function () {

	function init () {

		setup();
		animate();
		addEvents();
	}

	//
	// ───────────────────────────────────────────────────── SETUP DYNAMIC VALUES ─────
	// This is where you assign the dynamic values to your DOM elements.
    
	function setup () {

		/** Logo Image */
		var logoImgConfig = Utils.parseJSON( Dynamic.get().logo_img_config );
		DOM.get().logoImg.setAttribute( 'src', Dynamic.get().logo_img.Url );
		DOM.get().logoImg.style.left = logoImgConfig.xPos;
		DOM.get().logoImg.style.top = logoImgConfig.yPos;

		/** Frame 1 BG Image */
		var f1BgImgConfig = Utils.parseJSON( Dynamic.get().f1_bg_img_config );
		DOM.get().f1BgImg.style.backgroundImage = 'url(' + Dynamic.get().f1_bg_img.Url + ')';
		DOM.get().f1BgImg.style.backgroundPosition = f1BgImgConfig.xPos + ' ' + f1BgImgConfig.yPos;

		/** Frame 1 Copy */
		var f1Copy1Config = Utils.parseJSON( Dynamic.get().f1_copy1_config );
		DOM.get().f1Copy1.innerHTML = Dynamic.get().f1_copy1;
		DOM.get().f1Copy1.style.fontSize = f1Copy1Config.txtSize;
		DOM.get().f1Copy1.style.left = f1Copy1Config.xPos;
		DOM.get().f1Copy1.style.top = f1Copy1Config.yPos;
		DOM.get().f1Copy1.style.color = f1Copy1Config.txtColor;

		var f1Copy2Config = Utils.parseJSON( Dynamic.get().f1_copy2_config );
		DOM.get().f1Copy2.innerHTML = Dynamic.get().f1_copy2;
		DOM.get().f1Copy2.style.fontSize = f1Copy2Config.txtSize;
		DOM.get().f1Copy2.style.left = f1Copy2Config.xPos;
		DOM.get().f1Copy2.style.top = DOM.get().f1Copy1.offsetTop + DOM.get().f1Copy1.offsetHeight + parseInt( f1Copy2Config.yPos ) + 'px';
		DOM.get().f1Copy2.style.color = f1Copy2Config.txtColor;

		/** Frame 2 BG Image */
		var f2BgImgConfig = Utils.parseJSON( Dynamic.get().f2_bg_img_config );
		DOM.get().f2BgImg.style.backgroundImage = 'url(' + Dynamic.get().f2_bg_img.Url + ')';
		DOM.get().f2BgImg.style.backgroundPosition = f2BgImgConfig.xPos + ' ' + f2BgImgConfig.yPos;

		/** Frame 2 Copy */
		var f2CopyConfig = Utils.parseJSON( Dynamic.get().f2_copy_config );
		DOM.get().f2Copy.innerHTML = Dynamic.get().f2_copy;
		DOM.get().f2Copy.style.fontSize = f2CopyConfig.txtSize;
		DOM.get().f2Copy.style.left = f2CopyConfig.xPos;
		DOM.get().f2Copy.style.top = f2CopyConfig.yPos;
		DOM.get().f2Copy.style.color = f2CopyConfig.txtColor;
		DOM.get().f2Copy.querySelector( 'span' ).style.color = f2CopyConfig.colorAlt;

		/** Frame 3 Icon Image */
		var iconImgConfig = Utils.parseJSON( Dynamic.get().f3_icon_img_config );
		DOM.get().iconImg.setAttribute( 'src', Dynamic.get().f3_icon_img.Url );
		DOM.get().iconImg.style.left = iconImgConfig.xPos;
		DOM.get().iconImg.style.top = iconImgConfig.yPos;

		/** CTA */
		var ctaConfig = Utils.parseJSON( Dynamic.get().cta_config );
		DOM.get().cta.innerHTML = Dynamic.get().cta_copy;
		DOM.get().cta.style.backgroundColor = ctaConfig.bgColor;
		DOM.get().cta.style.fontSize = ctaConfig.txtSize;
		DOM.get().cta.style.left = ctaConfig.xPos;
		DOM.get().cta.style.top = ctaConfig.yPos;
		DOM.get().cta.style.color = ctaConfig.txtColor;
		DOM.get().cta.style.padding = ctaConfig.yPad + ' ' + ctaConfig.xPad;

		/** Pixels */
		DOM.get().pixels.setAttribute( 'src', 'images/pixels.png' );
	}

	//
	// ──────────────────────────────────────────────────────────────── ANIMATION ─────
	// This is where you'll place all animation related code.

	function animate () {
            
		/** Animation start values. */
		TweenMax.set( DOM.get().f1BgImg, { autoAlpha : 0 } );
		TweenMax.set( DOM.get().f2BgImg, { autoAlpha : 0 } );
		TweenMax.set( DOM.get().f1Copy1, { autoAlpha : 0 } );
		TweenMax.set( DOM.get().f1Copy2, { autoAlpha : 0 } );
		TweenMax.set( DOM.get().f2Copy, { autoAlpha : 0 } );
		TweenMax.set( DOM.get().iconImg, { autoAlpha : 0 } );
		TweenMax.set( DOM.get().cta, { autoAlpha : 0 } );
		TweenMax.set( DOM.get().pixels, { autoAlpha : 0, y : 200 } );

		/** Main animation code. */
		var tl = new TimelineMax();

		/** Fade in frame 1 image. */
		tl.to( DOM.get().f1BgImg, 2, { autoAlpha : 1, ease : Power2.easeInOut } );
		/** Fade in frame 1 copy 1. */
		tl.to( DOM.get().f1Copy1, 1, { autoAlpha : 1, ease : Power2.easeInOut }, '-=2' );
		/** Fade in frame 2 bg image and at the same time fade out frame 1 bg image. */
		tl.to( DOM.get().f2BgImg, 2, { autoAlpha : 1, ease : Power2.easeInOut }, '+=1' );
		tl.to( DOM.get().f1BgImg, 2, { autoAlpha : 0, ease : Power2.easeInOut }, '-=2' );
		/** Fade in and move the pixels. */
		tl.add( function () { TweenMax.to( DOM.get().pixels, 5, { autoAlpha : 1, y : 0 } ); }  );
		/** Change color of background to dark purple. */
		tl.set( DOM.get().wrapper, { backgroundColor : '#594ab8' } );
		/** Fade in frame 1 copy 2.  */
		tl.to( DOM.get().f1Copy2, 1, { autoAlpha : 1, ease : Power2.easeInOut }, '-=1.8' );
		/** Fade out frame 1 copy 1 and copy 2. */
		tl.to( [ DOM.get().f1Copy1, DOM.get().f1Copy2 ], 1, { autoAlpha : 0, ease : Power2.easeInOut }, '+=1' );
		/** Fade out frame 2 bg image. */
		tl.to( DOM.get().f2BgImg, 1, { autoAlpha : 0, ease : Power2.easeInOut }, '-=0.5' );
		/** Fade in frame 2 copy and cta. */
		tl.to( [ DOM.get().f2Copy, DOM.get().cta ], 1, { autoAlpha : 1, ease : Power2.easeInOut }, '-=0.5' );
		/** Fade in icon image (only for 160x600 and 300x600). */
		tl.to( DOM.get().iconImg, 1, { autoAlpha : 1, ease : Power2.easeInOut } );
	}

	//
	// ─────────────────────────────────────────────────────────────────── EVENTS ─────
	// This is where you'll place all event related codes (ie: Exits)

	function addEvents () {

		DOM.get().cta.addEventListener( 'click', function () {
            
			var exitURL = Utils.exitSuffix( Dynamic.get().exit_url.Url, 'exit_suffix' );
			Enabler.exitOverride( 'CTA_EXIT', exitURL );
		} );

		DOM.get().bgHotspot.addEventListener( 'click', function () {
            
			var exitURL = Utils.exitSuffix( Dynamic.get().exit_url.Url, 'exit_suffix' );
			Enabler.exitOverride( 'BG_EXIT', exitURL );
		} );
	}

	return {
        
		init : init
	};
}() );

//
// ────────────────────────────────────────────────────────── I ──────────
//   :::::: U T I L I T I E S : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────
//

var Utils = ( function () {

	//
	// ─── QUERY SELECTOR ─────────────────────────────────────────────────────────────
	//
 
	function selector ( query ) {
 
		var t = document.querySelectorAll( query );
		return ( t.length === 0 ) ? false : ( t.length === 1 ) ? t[0] : t;
 
	}
    
	//
	// ─── TRIGGER CUSTOM EVENT ───────────────────────────────────────────────────────
	//

	function triggerEvent ( element, eventName, data ) {
		if ( document.createEvent ) {
			var event = document.createEvent( 'CustomEvent' );
			event.initCustomEvent( eventName, true, true, data );
		} else {
			// eslint-disable-next-line no-redeclare
			var event = new CustomEvent( eventName, { detail : data } );
		}
    
		element.dispatchEvent( event );
	}

	//
	// ─── NORMALIZE JSON ─────────────────────────────────────────────────────────────
	//

	function parseJSON ( str ) {

		return JSON.parse( normalizeJSON( str ) );

	}

	function normalizeJSON ( str ) {
        
		return str.replace( /"?([\w_\- ]+)"?\s*?:\s*?"?(.*?)"?\s*?([,}\]])/gsi, ( str, index, item, end ) => '"' + index.replace( /"/gsi, '' ).trim() + '":"' + item.replace( /"/gsi, '' ).trim() + '"' + end ).replace( /,\s*?([}\]])/gsi, '$1' );
	}

	//
	// ─── DYNAMIC EXIT SUFFIX ────────────────────────────────────────────────────────
	//

	function exitSuffix ( url, dcmParameter ) { 

		var urlSuffix = Enabler.getParameter( dcmParameter ); 
		var _url = ''; 
      
		if ( url ) { 

			var symbol = ( url.indexOf( '?' ) > -1 ) ? '&' : '?'; 

			if ( urlSuffix ) { 

				while ( urlSuffix.charAt( 0 ) == '?' || urlSuffix.charAt( 0 ) == '&' ) { 

					urlSuffix = urlSuffix.substring( 1 ); 
				} 

				if ( urlSuffix.indexOf( '?' ) > -1 ) { 

					urlSuffix = urlSuffix.replace( /\?/g, '&' ); 
				} 
			} 
			_url = url + symbol + urlSuffix; 
		} 
		return _url; 
	} 
  
	return {
 
		selector : selector,
		triggerEvent : triggerEvent,
		normalizeJSON : normalizeJSON,
		parseJSON : parseJSON,
		exitSuffix : exitSuffix
	};
 
} )();